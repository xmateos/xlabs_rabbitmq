<?php

namespace XLabs\RabbitMQBundle\RabbitMQ;

use Psr\Log\LoggerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

abstract class Producer
{
    private $rabbitmq_settings;
    protected $logger;
    private $connection;
    private $channel;
    private $queue_name;

    public function __construct(LoggerInterface $logger, $xlabs_rabbitmq_config)
    {
        $this->logger = $logger;
        $this->rabbitmq_settings = $xlabs_rabbitmq_config;
    }

    public function process($data)
    {
        //$msg = new \stdClass();
        //isset($data['task_id']) ? $msg->task_id = $data['task_id'] : false;

        $this->connect($this->rabbitmq_settings['queue_prefix'].$this::getQueueName());

        if(method_exists($this, '_process'))
        {
            $this->_process($data);
        }

        $this->publish($data);

        $this->disconnect();
    }

    public function connect($queue_name)
    {
        $this->queue_name = $queue_name;
        $this->connection = new AMQPStreamConnection(
            $this->rabbitmq_settings['host'],
            $this->rabbitmq_settings['port'],
            $this->rabbitmq_settings['user'],
            $this->rabbitmq_settings['password'],
            $this->rabbitmq_settings['vhost']
        );
        $this->channel = $this->connection->channel();
        $this->channel->exchange_declare($this->rabbitmq_settings['exchange'], 'direct', false, true, false);
        //$this->channel->queue_declare($this->queue_name, false, true, false, false, false);
        //$queue_routing_key = $this->queue_name;
        //$this->channel->queue_bind($this->queue_name, $this->rabbitmq_settings['exchange'], $queue_routing_key);
    }

    public function publish($msg)
    {
        $msg_params = array(
            'delivery_mode' => 2,
            //'priority' => 1,
            //'timestamp' => time(),
            //'expiration' => strval(1000 * (strtotime('+1 day midnight') - time() - 1))
        );
        if(array_key_exists('priority', $msg))
        {
            $msg_params['priority'] = $msg['priority'];
        }
        $msg = new AMQPMessage(json_encode($msg), $msg_params);
        $this->channel->basic_publish($msg, $this->rabbitmq_settings['exchange'], $this->queue_name);
    }

    public function disconnect()
    {
        $this->channel->close();
        $this->connection->close();
    }
}