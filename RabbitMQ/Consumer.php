<?php

namespace XLabs\RabbitMQBundle\RabbitMQ;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Consumer extends Command
{
    protected $channel;
    protected static $consumer = 'xlabs_task:null_command';
    protected $messages_processed = 0;
    protected $start;
    protected $ttl = 0;
    protected $input;
    protected $output;
    /**
     * Messages that will be processed before canceling the queue to avoid running out of memory
     */
    protected $max_messages;

    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $rabbitmq_settings = $container->getParameter('xlabs_rabbitmq_config');

        $this->input = $input;
        $this->output = $output;

        $queue_name = $rabbitmq_settings['queue_prefix'].$this::getQueueName();

        $connection = new AMQPStreamConnection(
            $rabbitmq_settings['host'],
            $rabbitmq_settings['port'],
            $rabbitmq_settings['user'],
            $rabbitmq_settings['password'],
            $rabbitmq_settings['vhost']
        );
        $channel = $connection->channel();
        $this->channel = $channel;
        $this->channel->exchange_declare($rabbitmq_settings['exchange'], 'direct', false, true, false);
        $this->channel->queue_declare($queue_name, false, true, false, false, false);
        $queue_routing_key = $queue_name;
        $this->channel->queue_bind($queue_name, $rabbitmq_settings['exchange'], $queue_routing_key);
        $this->channel->basic_qos(null, 1, null);
        $this->start = time();
        $this->channel->basic_consume($queue_name, '', false, false, false, false, function($msg) use ($channel){
            /*
             * echo ONLY when consumers are run from the console with non www-data user
             */
            /*echo "\n".$queue_name." - ".date('H:i:s d-m-Y')."\n";
            echo "--------------------------------------------\n";
            foreach(json_decode($msg->body) as $i => $tracker_item)
            {
                echo $i." : ".$tracker_item."\n";
            }*/

            $callback_response = $this->callback($msg);
            $channel->basic_ack($msg->delivery_info['delivery_tag']);
            /*if($callback_response)
            {
                // ack
            } else {
                // nack/reject
            }*/

            // Respawn consumer if required
            //$this->respawn($msg);

            // stop consuming when ttl is reached
            if($this->ttl > 0 && (($this->start + $this->ttl) < time()))
            {
                $channel->basic_cancel($msg->delivery_info['consumer_tag']);
            }
        });
        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $this->channel->close();
        $connection->close();
    }

    /*protected function respawn($msg)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        if(isset($this->max_messages))
        {
            $this->messages_processed = $this->messages_processed + 1;
            if($this->messages_processed == $this->max_messages)
            {
                // Respawn consumer
                $this->channel->basic_cancel($msg->delivery_info['consumer_tag']);
                $container->get('xlabs_settings')->respawn($this::$consumer);
            }
        }
    }*/
}