<?php

namespace XLabs\RabbitMQBundle\API;

class RabbitMQAPI
{
    protected $config;
    private static $vhost = '/';
    /*private static $server = '192.168.5.26';
    private static $port = '15672';
    private static $username = 'admin';
    private static $password = 'admin';*/

    public function __construct($xlabs_rabbitmq_config)
    {
        $this->config = $xlabs_rabbitmq_config;
    }

    public function _get($api_uri)
    {
        $url = 'http://'.$this->config['user'].':'.$this->config['password'].'@'.$this->config['host'].':'.$this->config['api_port'].'/api/'.$api_uri;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_HEADER, false);
        //curl_setopt($ch, CURLOPT_USERPWD, self::$username.':'.self::$password);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        //curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $headers = array('Content-Type : application/x-www-form-urlencoded');
        curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);

        $curlErrno = curl_errno($ch);
        if($curlErrno)
        {
            dump(curl_error($ch));
            return false;
        }

        curl_close($ch);
        return $response;
    }

    public function getQueues()
    {
        return $this->_get('queues/'.urlencode($this->config['vhost']));
    }

    public function getConsumers()
    {
        // THIS DOESNT WORK (even being in documentation). PROBABLY NEEDS RABBITMQ UPGRADE
        return $this->_get('consumers/'.urlencode($this->config['vhost']));
    }
}